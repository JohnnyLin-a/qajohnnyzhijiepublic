package ca.johnnyzhijie.qajohnnyzhijie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class CategoryAdaptor extends BaseAdapter {
    List<Category> categories;

    MainActivity context;
    LayoutInflater inflater;

    public CategoryAdaptor(MainActivity mainActivity, List<Category> categories){
        this.categories = categories;
        this.context = mainActivity;
        inflater = context.getLayoutInflater();
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final View rowView;
        rowView = inflater.inflate(R.layout.category_item, null);
        TextView tv = rowView.findViewById(R.id.category_title);
        tv.setText(categories.get(i).name);
        ImageView iv = rowView.findViewById(R.id.category_image);
        iv.setImageDrawable(categories.get(i).getImageView().getDrawable());
        final String categoryName = categories.get(i).name;
        final Drawable drawable = categories.get(i).getImageView().getDrawable();
        Bitmap bitmap = ((BitmapDrawable)drawable).getBitmap();
        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        rowView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(rowView.getContext(), QAActivity.class);
                intent.putExtra("category", categoryName);
                intent.putExtra("categoryImage",
                        baos.toByteArray());
                rowView.getContext().startActivity(intent);
            }
        });
        return rowView;
    }
}
