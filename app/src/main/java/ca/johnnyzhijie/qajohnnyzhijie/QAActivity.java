package ca.johnnyzhijie.qajohnnyzhijie;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.solver.widgets.Snapshot;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This class displays the short questions from a specific category and displays them in a
 * ListView.
 *
 * @author Johnny
 */
public class QAActivity extends MenuActivity {
    private DatabaseReference mQuestionsReference;
    private String category = "Video Games";
    private HashMap<String, DataSnapshot> snapshots;
    private byte[] image;
    private final String TAG = "QAActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qa);

    }

    /**
     * Method gets the category from the past activity.
     * The title will display the category name.
     * The Firebase database will be queried and will display the short questions in a ListView
     */
    @Override
    public void onStart() {
        super.onStart();
        this.category = getIntent().getStringExtra("category");
        this.image = getIntent().getByteArrayExtra("categoryImage");
        ((TextView)findViewById(R.id.qa_title)).setText(this.category);
        //get Firebase data and set them.
        updateUI();
    }

    private void updateUI() {
        //update UI
        Log.d(TAG, "In updateUI");

        //get database instance
        mQuestionsReference = FirebaseDatabase.getInstance().getReference("Questions");

        //set database data change listener
        mQuestionsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                snapshots = new HashMap<>();

                //add each children short questions in shorts ArrayList
                Log.d(TAG,"Children Count: " + dataSnapshot.getChildrenCount());
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    //Log.d(TAG, "dataSnapshot1.child(\"category\").getValue() -> " + dataSnapshot1.child("category").getValue());
                    if(dataSnapshot1.child("category").getValue().equals(category)) {
                        Log.d(TAG, "Value in children is: " + dataSnapshot1.getKey() + " for category: " + dataSnapshot1.child("category").getValue());
                        snapshots.put(dataSnapshot1.getKey(), dataSnapshot1);
                    }
                }
                //Logs shorts in console
                for (String s : snapshots.keySet()) {
                    Log.d(TAG, "Shorts: " + s);
                }

                setListView();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        Log.d(TAG, "Finished updateUI");
    }

    private void setListView() {
        //set the list view data.
        ListView lv = findViewById(R.id.short_question_lv);
        List<String> shortNames = new ArrayList<>();
        for (String s : snapshots.keySet()) {
            shortNames.add(s);
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, shortNames);
        lv.setAdapter(arrayAdapter);

        //set the list view on item click listener.
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d(TAG,"Clicked: " +((TextView) view).getText());
                //set last Question (in shared prefs?)

                String name = (String)((TextView) view).getText();
                DataSnapshot snapshot = snapshots.get(name);
                String answer = snapshot.child("answer").getValue().toString();
                String category = snapshot.child("category").getValue().toString();
                String date = snapshot.child("date").getValue().toString();
                String question = snapshot.child("question").getValue().toString();
                String source = snapshot.child("source").getValue().toString();

                //set last run
                setLastRun(name, answer, category, date, question, source);
                //launch activity passing short in extras.
                Intent i = new Intent(QAActivity.this, QuestionActivity.class);
                i.putExtra("short", (String)((TextView) view).getText());
                i.putExtra("question", question);
                i.putExtra("answer", answer);
                i.putExtra("date", date);
                i.putExtra("category", category);
                i.putExtra("source", source);
                i.putExtra("categoryImage", image);
                startActivity(i);


            }
        });
    }

    private void setLastRun(String name, String answer, String category, String date, String question, String source) {
        //sets the last question activity in a shared preference.
        SharedPreferences prefs = getSharedPreferences("lastRun" ,MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.putString("name", name);
        editor.putString("answer", answer);
        editor.putString("category", category);
        editor.putString("date", date);
        editor.putString("question", question);
        editor.putString("source", source);

        editor.commit();
    }

}
