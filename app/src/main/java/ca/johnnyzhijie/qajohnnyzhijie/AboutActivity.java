package ca.johnnyzhijie.qajohnnyzhijie;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

/**
 * Class responsible for displaying descriptions and images about our app
 *
 * @author Johnny
 */
public class AboutActivity extends MenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
    }
}
