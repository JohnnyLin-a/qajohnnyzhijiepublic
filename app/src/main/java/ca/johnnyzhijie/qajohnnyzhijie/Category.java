package ca.johnnyzhijie.qajohnnyzhijie;

import android.widget.ImageView;

/**
 * This class is used to store the data fetched from firebase.
 */
public class Category {
    public String name;
    public String img;
    private ImageView imageView;

    public Category(){

    }

    /**
     * Category constructor
     * @param name
     * @param src
     */
    public Category(String name, String src) {
        this.name = name;
        this.img = src;
    }

    public void setImageView(ImageView image){
        this.imageView = image;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
