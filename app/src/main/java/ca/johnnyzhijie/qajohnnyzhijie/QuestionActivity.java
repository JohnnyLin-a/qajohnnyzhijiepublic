package ca.johnnyzhijie.qajohnnyzhijie;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class QuestionActivity extends MenuActivity {
    String longQuestion;
    String answer;
    String date;
    String categoryName;
    String source;
    String shortQuestion;
    byte[] categoryImage;

    /**
     * Creates the view and fetch information from the intent.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        Bundle bundle = getIntent().getExtras();
        shortQuestion = bundle.getString("short");
        longQuestion = bundle.getString("question");
        answer =  bundle.getString("answer");
        date = bundle.getString("date");
        categoryName = bundle.getString("category");
        source = bundle.getString("source");
        categoryImage = bundle.getByteArray("categoryImage");

        TextView question = findViewById(R.id.Question);
        TextView dateView = findViewById(R.id.date);
        TextView category = findViewById(R.id.category_title);
        category.setText(categoryName);
        dateView.setText(date);
        question.setText(longQuestion);
        Button source = findViewById(R.id.source);
        source.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/JohnnyLin.LYA"));
                startActivity(browserIntent);
            }
        });
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(QuestionActivity.this);
                builder.setMessage(answer)
                        .setTitle(R.string.answer);


                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword("zhijiec99@hotmail.com", "android")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Test", "signInWithEmail:success");
                            fetchCategoryData();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("test", "signInWithEmail:failure", task.getException());
                            Toast.makeText(QuestionActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    /**
     * When the app is resumed, set all the text views to their values
     */
    @Override
    protected void onResume() {
        super.onResume();
        fetchCategoryData();


    }

    /**
     * fetches the images based on a url
     * @param url the url to fetch
     */
    private void fetchImages(final String url) {
        final FirebaseAuth mAuth = FirebaseAuth.getInstance();

        StorageReference storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(url);
        final long TEN_MEGABYTE = 10 * 1024 * 1024;

        storageRef.getBytes(TEN_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
            @Override
            public void onSuccess(byte[] bytes) {
                Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                ImageView imageView = findViewById(R.id.category_image);
                imageView.setImageBitmap(bmp);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Toast.makeText(QuestionActivity.this, "Failed to retrieve image", Toast.LENGTH_SHORT).show();
            }
        });

    // ...

    }

    /**
     * fetches a the category
     */
    private void fetchCategoryData() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("Categories");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            String TAG = "TEST";
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getKey();
                Log.d(TAG ,"Children Count: " + dataSnapshot.getChildrenCount());

                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    if(dataSnapshot1.getKey().equals(categoryName)){
                        Category category = dataSnapshot1.getValue(Category.class);
                        fetchImages(category.img);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }
}
