package ca.johnnyzhijie.qajohnnyzhijie;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible of displaying the OptionsMenu on the top right of the app's activities
 * user interfaces.
 *
 * @author Johnny
 */
public class MenuActivity extends Activity {
    DatabaseReference mQuestionsReference;

    private static final String TAG = "MenuActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    /**
     * Method inflates option menus in the Activities' UI.
     * @param menu menu
     * @return bool
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Method that will handle selected OptionMenu items.
     * About will launch the about activity.
     * Random will launch a random question activity.
     * Last will launch the last run question activity.
     *
     * @param item menu item
     * @return bool
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //handle clicked item
        switch (id) {
            case R.id.menu_about:
                startAbout();
                break;
            case R.id.menu_random:
                randomRun();
                break;
            case R.id.menu_last:
                lastRun();
                break;
            default:
                Toast.makeText(this, "Invalid menu item", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void startAbout() {
        //start about activity
        startActivity(new Intent(this, AboutActivity.class));
    }

    private void randomRun() {
        //get database reference
        mQuestionsReference = FirebaseDatabase.getInstance().getReference("Questions");
        mQuestionsReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //get all shorts
                List<DataSnapshot> questions = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    questions.add(dataSnapshot1);
                }

                //random a selection
                java.util.Random rand = new java.util.Random();
                int random = rand.nextInt(questions.size());


                //record last run from the random
                DataSnapshot question = questions.get(random);
                SharedPreferences sharedPreferences = getSharedPreferences("lastRun", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();

                editor.clear();
                editor.putString("name", question.getKey());
                editor.putString("answer", question.child("answer").getValue().toString());
                editor.putString("category", question.child("category").getValue().toString());
                editor.putString("date", question.child("date").getValue().toString());
                editor.putString("question", question.child("question").getValue().toString());
                editor.putString("source", question.child("source").getValue().toString());
                editor.commit();

                Log.d(TAG, "Name: "+ question.getKey());
                Log.d(TAG, "answer: "+ question.child("answer").getValue().toString());
                Log.d(TAG, "category: "+ question.child("category").getValue().toString());
                Log.d(TAG, "date: "+ question.child("date").getValue().toString());
                Log.d(TAG, "question: "+ question.child("question").getValue().toString());
                Log.d(TAG, "source: "+ question.child("source").getValue().toString());

                //launch new Question Activity
                Intent i = new Intent(MenuActivity.this, QuestionActivity.class);
                i.putExtra("short", question.getKey());
                i.putExtra("answer", question.child("answer").getValue().toString());
                i.putExtra("category", question.child("category").getValue().toString());
                i.putExtra("date", question.child("date").getValue().toString());
                i.putExtra("question", question.child("question").getValue().toString());
                i.putExtra("source", question.child("source").getValue().toString());
                startActivity(i);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    private void lastRun() {
        //get last run from shared prefs
        SharedPreferences prefs = getSharedPreferences("lastRun", MODE_PRIVATE);
        String name = prefs.getString("name", null);

        if (name != null) {
            String answer = prefs.getString("answer", null);
            String category = prefs.getString("category", null);
            String date = prefs.getString("date", null);
            String question = prefs.getString("question", null);
            String source = prefs.getString("source", null);
            //run Question Activity with lastRun as Extra.
            Intent i = new Intent(this, QuestionActivity.class);
            i.putExtra("short", name);
            i.putExtra("question", question);
            i.putExtra("answer", answer);
            i.putExtra("date", date);
            i.putExtra("category", category);
            i.putExtra("source", source);
            i.putExtra("categoryImage", "");
            startActivity(i);
        } else {
            //Toast if there is no last run
            Toast.makeText(this, "There is no last run.", Toast.LENGTH_SHORT).show();
        }
    }
}
