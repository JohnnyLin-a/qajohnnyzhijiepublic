package ca.johnnyzhijie.qajohnnyzhijie;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends MenuActivity {
    private FirebaseAuth mAuth;
    private FirebaseDatabase database;
    StorageReference storageRef;
    private List<Category> categories;
    private List<ImageView> images;

    /**
     * Calls when Created
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Choose authentication providers

    }

    /**
     * Called on start
     */
    @Override
    protected void onStart(){
        super.onStart();

    }

    /**
     * Called when resumed, authenticates the user
     */
    @Override
    protected void onResume() {
        super.onResume();
        mAuth = FirebaseAuth.getInstance();

        mAuth.signInWithEmailAndPassword("zhijiec99@hotmail.com", "android")
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Test", "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            fetchCategoryData();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("test", "signInWithEmail:failure", task.getException());
                            Toast.makeText(MainActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
        ListView lv = findViewById(R.id.category_list);
        lv.setClickable(true);
    }

    /**
     * Fetches all the data from a category
     * @Author Zhi Jie Cao
     */
    private void fetchCategoryData() {
        database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference("Categories");
        ref.addListenerForSingleValueEvent(new ValueEventListener() {
            String TAG = "TEST";
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String value = dataSnapshot.getKey();
                Log.d(TAG ,"Children Count: " + dataSnapshot.getChildrenCount());
                Log.d(TAG,"Value is: " + value);
                categories = new ArrayList<>();
                for (DataSnapshot dataSnapshot1 : dataSnapshot.getChildren()) {
                    Category category = dataSnapshot1.getValue(Category.class);
                    categories.add(category);
                }
                fetchImages(categories);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }
        });
    }

    /**
     * Fetches the images from the database from a list of categories
     * @param categories
     */
    private void fetchImages(final List<Category> categories) {
        images = new ArrayList<>();
        for (int i = 0; i < categories.size(); i++) {
            storageRef = FirebaseStorage.getInstance().getReferenceFromUrl(categories.get(i).img);
            final Category category = categories.get(i);
            final long TEN_MEGABYTE = 10 * 1024 * 1024;

            storageRef.getBytes(TEN_MEGABYTE).addOnSuccessListener(new OnSuccessListener<byte[]>() {
                @Override
                public void onSuccess(byte[] bytes) {
                    Bitmap bmp = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    ImageView imageView = new ImageView(MainActivity.this);
                    imageView.setImageBitmap(bmp);
                    category.setImageView(imageView);
                    images.add(imageView);
                    if(images.size() == categories.size()){
                        populateListView();
                   }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Toast.makeText(MainActivity.this, "Failed to retrieve image", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Populates the list view with the
     * @Author Zhi Jie Cao
     */
    public void populateListView() {
        ListView lv = findViewById(R.id.category_list);
        lv.setClickable(true);

        lv.setAdapter(new CategoryAdaptor(this, categories));
    }
}
